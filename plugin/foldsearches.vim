
" HEADER

" Description: Vim global plugin for folding text around search results
" Authored: Damian Conway
" Edited: dsjkvf (dsjkvf@gmail.com)
" License: This file is placed in the public domain.

" INIT

" Check if loaded
if exists("loaded_foldsearch")
    finish
endif
let loaded_foldsearch = 1

" Preserve external compatibility options, then enable full vim compatibility
let s:save_cpo = &cpo
set cpo&vim

" Remember default behaviour
let s:DEFFOLDMETHOD = &foldmethod
let s:DEFFOLDEXPR   = &foldexpr
let s:DEFFOLDTEXT   = &foldtext
let s:DEFFOLDLEVEL  = 1000

" Check and set the main options
if !exists('g:FS_visible_folds')
    let g:FS_visible_folds = 1
endif
let s:FOLDEXPR = 'FS_FoldSearchLevel()'

" MAIN

" Turn the mechanism on and off
function! FS_ToggleFoldAroundSearch()
    " make sure we can remember the previous setup
    if !exists('b:foldsearch')
        let b:foldsearch = { 'active' : 0 }
    endif
    " turn off, if it's on
    if b:foldsearch.active
        let &foldmethod = get(b:foldsearch, 'prevfoldmethod', s:DEFFOLDMETHOD)
        let &foldtext   = get(b:foldsearch, 'prevfoldtext',   s:DEFFOLDTEXT)
        let &foldlevel  = get(b:foldsearch, 'prevfoldlevel',  s:DEFFOLDLEVEL)
        let &foldexpr   = get(b:foldsearch, 'prevfoldexpr',   s:DEFFOLDEXPR)
        " remove autocommands for refolding for each new search
        augroup FoldSearch
            autocmd!
        augroup END
        " remember that it's off
        let b:foldsearch.active = 0
        " disable special <CR> behaviour
        nunmap <buffer> <CR>
        return ":foldopen\!\<CR>"
    " turn on, if it's off
    else
        " save old settings
        let b:foldsearch.prevfoldmethod = &foldmethod
        let b:foldsearch.prevfoldexpr   = &foldexpr
        let b:foldsearch.prevfoldtext   = &foldtext
        let b:foldsearch.prevfoldlevel  = &foldlevel
        " set up new behaviour
        if g:FS_visible_folds == 1
            let &foldtext = "'(' . (v:foldend - v:foldstart + 1) . ')'"
        elseif g:FS_visible_folds == 0
            let &foldtext = "repeat(' ',200)"
        else
            echoerr "Folds visibility is undefined; please, check the manual."
        endif
        let &foldexpr   = s:FOLDEXPR
        let &foldmethod = 'expr'
        let &foldlevel  = 0
        " recalculate folding for each new search
        augroup FoldSearch
            autocmd!
            autocmd CursorMoved  *  let b:inopenfold = foldlevel('.') && foldclosed('.') == -1
            autocmd CursorMoved  *  let &foldexpr  = &foldexpr
            autocmd CursorMoved  *  let &foldlevel = 0
            autocmd CursorMoved  *  call ReopenFold()
            function! ReopenFold ()
                if b:inopenfold
                    normal zo
                endif
            endfunction
        augroup END
        " enable special <CR> behaviour
        nnoremap <buffer> <expr> <CR> foldlevel('.') ? 'zA' : ''
        " remember that it's on
        let b:foldsearch.active = 1
        return ":nohlsearch\<CR>\<C-L>"
    endif
endfunction

" Utility function to implement folding expression
function! FS_FoldSearchLevel()
    " define context
    let startline = v:lnum
    let endline = v:lnum
    " get the contents
    let line_text = getline(v:lnum)
    "  compare to comment_leader
    if b:comment_leader != ""
        if match(line_text, b:comment_leader) != -1
            " expand the range if possible
            let endline = v:lnum + 1
        endif
    endif
    " define the context by the range
    let context = getline(startline, endline)
    " simulate smartcase matching
    let matchpattern = @/
    if &smartcase && matchpattern =~ '\u'
        let matchpattern = '\C'.matchpattern
    endif
    " line is folded if surrounding context doesn't match last search pattern
    return match(context, matchpattern) == -1
endfunction

" FINI

" Restore previous external compatibility options
let &cpo = s:save_cpo
