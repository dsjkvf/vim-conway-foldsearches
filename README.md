# Folding around search results

## About

This is a Vim plugin for folding text around search results. Originally written by [Damian Conway](http://damian.conway.org/About_us/Bio_formal.html), forked now from [Damian's collection](https://github.com/thoughtstream/Damian-Conway-s-Vim-Setup) of his Vim-related files because of some simplification edits and in order to make the installation process easier.

## Changes

The resulting plugin now folds everything around the search results, **and** if a line above any particular search result in a buffer is a comment line, then excludes that line from folding, too (shows it above that search result) – here's an example search for `def `:

![visible](http://i.imgur.com/i9XXuoE.png)

To detect what is a comment line, and what is not, the plugin compares the lines above search results to a buffer variable `b:comment_leader`, which is to be set via corresponding `ftplugins` – like this (quoting `$VIMHOME/after/ftplugin/python.vim`):

    " define comment pattern
    let b:comment_leader = '# '

## Configuration

To toggle the folding around search results, add the following mapping to one's `.vimrc`:

    nnoremap <silent> <expr> <Leader>fs FS_ToggleFoldAroundSearch()

## Options

The only option available is the visibility (or not) of folds themselves. To hide those, add the following setting to one's `.vimrc` (default behaviour is visible):

    let g:FS_visible_folds = 0

Which tranforms the output like that:

![invisible](http://i.imgur.com/r7vmXVg.png)
